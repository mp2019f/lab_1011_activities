package com.gohool.showactivity.showactivity;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class FirstActivity extends AppCompatActivity {
    private Button goToSecondButton;
    private final int REQUEST_CODE = 2;
    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        //mp = new MediaPlayer();
        //mp = MediaPlayer.create( getApplicationContext(), R.raw.napal_baji );
/*        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setDataSource(getApplicationContext(), R.raw.napal_baji);
        mediaPlayer.prepare();
        mediaPlayer.start();
*/

        goToSecondButton = (Button) findViewById(R.id.showButtonID);
        goToSecondButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp = MediaPlayer.create(getApplicationContext(), R.raw.napal_baji );
                mp.start(); // no need to call prepare(); create() does that for you
                //mp.start();
                //Code goes here
        //        Intent intent = new Intent(FirstActivity.this, SecondActivity.class);
      //          intent.putExtra("Message", "Hello From First Activity");
    //            intent.putExtra("SecondMessage", "Hello Again");
  //              intent.putExtra("Value", 123);

                //startActivity(intent);
//                startActivityForResult(intent, REQUEST_CODE);
                //startActivity(new Intent(FirstActivity.this, SecondActivity.class));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
             if (resultCode == RESULT_OK) {
                  String result = data.getStringExtra("returnData");

                 Toast.makeText(FirstActivity.this, result, Toast.LENGTH_LONG).show();


             }
        }


    }
}
